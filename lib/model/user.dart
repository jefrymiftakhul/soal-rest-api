class User {
  final int id;
  final String email;
  final String first_name;
  final String last_name;
  final String avatar;

  User({this.id, this.email, this.first_name, this.last_name, this.avatar});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      email: json['email'],
      first_name: json['first_name'],
      last_name: json['last_name'],
      avatar: json['avatar'],
    );
  }

  Map<String, dynamic> toJson() => {
//'id': id,
    'email': email,
    'first_name': first_name,
    'last_name': last_name,
    'avatar': avatar,
  };
}
