import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../networkapi.dart';
import '../model/user.dart';

class Details extends StatefulWidget {
  final User user;

  Details({this.user});

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Details'),

      ),
      //resizeToAvoidBottomInset: false,
      body: Container(
        ///height: 270.0,
        padding: EdgeInsets.only(left:20,top: 20),
        //padding: const EdgeInsets.all(35),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Email : ${widget.user.email}",
              style: TextStyle(fontSize: 20),
            ),
            Text(
              "\n First Name : ${widget.user.first_name}",
              style: TextStyle(fontSize: 20),
            ),
            Text(
              "\n Last Name : ${widget.user.last_name}",
              style: TextStyle(fontSize: 20),
            ),
            Text(
              "\n Avatar",
              style: TextStyle(fontSize: 20),
            ),
            Image.network('${widget.user.avatar}'),
            //Image.network('${widget.user.avatar}', fit: BoxFit.fitWidth),
            /*Image.network(
                '${widget.user.avatar}',
                height: 150,
                width: MediaQuery.of(context).size.width,
                fit:BoxFit.cover

            ),*/

            Padding(
              padding: EdgeInsets.all(10),
            ),

          ],
        ),
      ),

    );
  }
}