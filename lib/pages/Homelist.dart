import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../networkapi.dart';
import '../model/user.dart';
import './details.dart';
//import './create.dart';

class Homelist extends StatefulWidget {
  Homelist({Key key}) : super(key: key);
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Homelist> {
  Future<List<User>> users;
  final userListKey = GlobalKey<HomeState>();

  @override
  void initState() {
    super.initState();
    users = getUserList();
  }

  Future<List<User>> getUserList() async {
    final response = await http.get(Uri.parse('$GETALLS'));
    final parsed = json.decode(response.body)['data'].cast<Map<String, dynamic>>();

    return parsed.map<User>((json) => User.fromJson(json)).toList();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: userListKey,
      appBar: AppBar(
        title: Text('User List'),
      ),
      body: Center(
        child: FutureBuilder<List<User>>(
          future: getUserList(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return CircularProgressIndicator();
            // Render genre lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    leading: CircleAvatar(
                      radius: 30,
                      backgroundImage:
                      NetworkImage(data.avatar),
                    ),
                    title: Text(
                      data.first_name,
                      style: TextStyle(fontSize: 20),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Details(user: data)),
                      );
                    },
                  ),
                );
              },
            );
          },
        ),
      ),

    );
  }
}