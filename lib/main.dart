import 'package:flutter/material.dart';

import './pages/Homelist.dart';
import './pages/details.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter + PHP CRUD',
      initialRoute: '/',
      routes: {
        '/': (context) => Homelist(),
        '/details': (context) => Details(),
      },
    );
  }
}
